## Upcomming v0.9.2:
- Added components for gaze stabilization
- Fixed object localization result. ImageProvider has to provide a timestamp with the current image.
- ImageProcessor::getImages() now requires a new output parameter, i.e. armarx::MetaInfoSizeBasePtr
- Correcting & updating documentation pages: Most pages of the documentation on https://armarx.humanoids.kit.edu/ offer now a link a the bottom called "Edit Page" to the source page on Gitlab - please use it!
- Observers save now a history of datafields of the last X updates
- Added troubleshooting tutorial: https://armarx.humanoids.kit.edu/ArmarXCore-Tutorial-Troubleshooting.html
- Software safety stop: Added interfaces and components for a software safety stop that is automatically shown in all GUI windows if robot supports it. Currently implemented for Armar3/4.
- Added HowTo for local installation of ArmarX: https://armarx.humanoids.kit.edu/ArmarXCore-Local-Installation.html
- Added timing helper macros for profiling: https://armarx.humanoids.kit.edu/group__VirtualTime.html#ga4f9b6b0295446bf1ab2fead881cc6f71

## v0.9.1:
- Switch of old CMake-scenario-format to new scenario-format of ScenarioManager
 - Generating scenarios via CMake must be enabled manually now
 - Detailed announcement: https://www.lists.kit.edu/sympa/arc/armarx/2016-10/msg00002.html
- TopicRecorder/Replayer was extended significantly: All recordings are now stored in a sqlite database. Interactive replaying with GUI including live replay speed change and jumping to arbitrary position
- ArmarX Python bindings: Conveniently connect from python to ArmarX interfaces (https://armarx.humanoids.kit.edu/armarx-python.html)
- *RelWithDebInfo* is now default build type
- RobotViewer gui plugin offers functionality to copy joint angles and poses in Statechart-JSON format now
- Default log verbosity is now *Info* instead of *Verbose* to reduce log output in default configuration
- The RobotStateComponent now publishes the root pose and the joint values of the robot on a topic (joint values are just a forwarding of the kinematic unit topic)
- ExternalApplicationManager will kill now all launched processes, even if it gets killed itself with signal 9
- ExternalApplicationManager can scan the output of the child for key words to trigger the start up of the dummy object (can be used to specify dependencies)
- CollisionChecker component that monitors the distances between robot and the environment (based on working memory content)
- Trajectory format and trajectory player was added to execute (recorded) trajectories on the robot
- Strings can now be plotted as vertical markers

## v0.8.8:
- Hotfix: RobotComponents did not install correctly

## v0.8.7:
- Ice 3.5.1 is now required for some features
- Simulator improvements: Simulation runs faster (real-time on average machines and typical object count) and objects are more stable; walking for biped robots also possible now
- VirtualTime: all components and statecharts are now making use of the ArmarX VirtualTime
- Topic Recording & Replaying: Any Ice topic can now be recorded to a file and played back again. See: http://armarx.humanoids.kit.edu/ArmarXCore-TopicRecording.html
- Memory Segment locking: Memory segments can now be locked to perform multiple queries atomically
- System State Monitor GUI: Stability, usability and responsiveness improvements
- ViewSelection split into ObjectLocalizationSaliency and ViewSelection; ViewSelection is moved to RobotAPI
- Unified typedefs of basic types (e.g. StringList to Ice::StringSeq)
- Gui plugin caching: Available Gui plugins are loaded only on request and their meta information is cached to reduce start up time
- Stream Provider re-worked: compression of images to an h264 encoded video stream + decoding to normal image provider for slow networks (compression with high quality from 30MB/s to 0.2MB/s for stereo images at 640x480@25fps)
- Image Monitor stability improvements and selection of shown images possible
- Platform keyboard control: The platform can now be controlled with the keyboard in the Platform Gui.
- Statechart Transition Hook: You can now add user-code that is executed during state transitions for parameter conversions etc.
- StatechartViewer supports connection to all running statecharts (instead of only to  meta statechart *RobotControl*) and triggering of events by right-clicking transitions
- StatechartEditor allows one-click execution of states with full configuration and automatic execution of all statechart dependencies
- Drag'N'Drop of statechart groups into scenarios of ScenarioManager
- Kinematic Bezier Maps can now be used as memory motion model 

## v0.8.6 
- This release is buggy and is retracted


## v0.8.5:
- Simplified JSON format for default values in Statechart Editor
- Statechart Viewer refresh and reconnect fixed
- Statechart Viewer added display of state parameters
- Added statechart and scenario tester
- New NLOpt IK of Simox is used in IK Component and IK Statecharts
- New GUI: DebugDrawerViewer - a GUI which displays the content of the DebugDrawer topic
- New GUI (Beta): ScenarioManager for creating, managing and executing scenarios (Will replace old CMake scenarios soon)
- Static robots in physics simulator can now be controlled in velocity mode

## v0.8.4:
- Hotfix: Read-only statechart groups lead to a buggy context menu in the Statechart Editor

## v0.8.3:
- RobotState history (the last x seconds of robot movement are stored and can be queried (with interpolation))
- Multi Robot Setup with individually configured statecharts for each robot possible
- Added YarpBridge package
- Updated PointCloudSegmenter component to PCL version 1.8.0rc2 
- MultiSense SL driver for VisionX
- Statechart Editor: Direct Mapping of values in transitions
- Statechart Editor: Configuration and starting of statechart groups from the editor (ArmarX/ArmarXGui#3)
- Kinematic Simulation: Fixed Platform Unit and added object and hand localizer based on the working memory itself. Also added full scenario in ArmarXSimulation named KinematicArmar3Simulation
- Extension of Graph gui: (Platform)-Graphs can now be edited and created from the GUI

## v0.8.2:
- Statechart Editor: Statechart Group View filterable
- MMM Model added to RobotAPI
- Static robot-mode in physics simulation (Immovable by other objects, but joints and position can be set externally on-the-fly)
- Multiple robots in physics simulation
- Simox version 2.3.27 required for MMM simulation

## v0.8.1:
- Hotfix: Save bug in StatechartEditor when using installed packages (fixes Issue ArmarX/ArmarXGui#4)
- New feature: Statechart Editor autolayouting
- New feature: Initial version of Statechart Viewer (unstable)
- Fix: Ghost transition bug (fixes Issue ArmarX/ArmarXGui#5)
- Fix: MemoryX DB import was executed wrong on some machines - fix added to import script

## v0.8.0:
- No Garbage Collection needed anymore
- Observer performance improved
- All standard skills use now by default synchronized position control with defined de/acceleration curve for smoother movements
- Added TimedVariant
- All Observer datafields are timestamped
- New Feature: Added ObjectLocalizationRequest Gui
- Statechart Editor respects read-only states
- New Feature: Object Shape Completion
- Redirection of std::cout and std::cerr to ArmarX Log
- Fixed Working Memory Gui crashes
- armarx cli: armarx-dec exec command for execution shell commands
- armarx cli: armarx memory command to start/stop memory
- Divided ~/.armarx/default.cfg into two files: ~/.armarx/default.cfg and ~/.armarx/default.generated.cfg
- Fixed RobotHand Motion Model in respect to orientation
