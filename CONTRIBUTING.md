# Contributing to ArmarX as a GitLab Team Member

## Developer

### Creating a Merge Request

1. clone ArmarX as indicated in [README.md](README.md)
2. create a branch in the specific ArmarX package with `git checkout -b <branchname>`
3. develop your feature and commit the changes to the branch (create multiple smaller commits if possible)
4. push the changes to a remote branch on GitLab with `git push -u origin <branchname>`
5. click the `Create Merge Request` button which appears on the top of the page when you visit https://gitlab.com/groups/ArmarX
6. check that your newly created branch is set as the source branch of the merge request
7. check that `master` is set as the target branch of the merge request
8. select your advisor as assignee (leave the field empty if unsure)
9. click the `SUBMIT NEW MERGE REQUEST` button

### After Creating a Merge Request

After submitting the merge request it might take a little bit for someone to review the code and accept the request.
Any comments, changes, or suggestions made on the request should be addressed.

It is no problem at all to push additional changes to the branch in the repository.
These changes are added to the merge request automatically.

#### Resolving Conflicts between master branch and your Merge Request

During the lifetime of a merge request, automatic merging via
GitLab web interface might be disabled due to merge conflicts.
Resolving these conflicts requires your branch to get in sync with the latest
`master` branch which can be achieved in two ways.

First update your `master` branch using `git checkout master & git pull`.
Then proceed with one of the following steps:

* unexpirienced Git users:
    1. `git checkout <your-merge-request-branch>`
    2. `git merge master`
    3. resolve conflicts and commit the changes
    4. `git push`

* experienced Git users (you should know what `git rebase` is and what effects it has):
    1. `git checkout <your-merge-request-branch>`
    2. `git rebase master` (requires resolving conflicts during rebase)
    3. `git push --force` (only do a `push --force` on merge request branches, where this is expected to happen)

## Repository Master/Owner

### Reviewing Merge Requests

1. go to the repository of the specific ArmarX package
2. open the merge request and check the description and the changes (diff view)
3. leave general comments on the `Discussion` tab if something is missing, wrong, or needs to be changed
4. leave more detailed comments on specific lines of the sourcecode in the `Changes` tab
5. once everything is ready to be merged click the `Remove source branch` checkbox and afterwards the `ACCEPT MERGE REQUEST` button

### Releasing a new ArmarX version

1. check out the `stable` branch in each repository (e.g. `git submodule foreach 'git checkout stable'`)
2. merge `master` branch, hotfix branches, or cherry-pick specific commits into the `stable` branch
3. use the `armarx_version.py` script from `ArmarXCore/etc/python/armarx/` to increase the version number and create a version tag (requires clean repos): `armarx_version.py -b (major|minor|patch)`
4. for each repository: `git push origin stable && git push --tags origin stable`
5. armarx umbrella project: update and commit the updated submodules
6. armarx umbrella project: create a tag matching the new stable ArmarX version: `git tag -a vX.X.X -m "ArmarX vX.X.X"`
7. armarx umbrella project: `git push origin stable && git push --tags origin stable`
8. check out the `master` branch in each repository
9. for each repository: `git merge stable && git push origin master`
10. build the Ubuntu packages