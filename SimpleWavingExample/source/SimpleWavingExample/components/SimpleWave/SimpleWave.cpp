/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SimpleWavingExample::ArmarXObjects::SimpleWave
 * @author     [Author Name] ( [Author Email] )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimpleWave.h"


using namespace armarx;


void SimpleWave::onInitComponent()
{
    positionToReach = &JointValueMapInitPose;
    nextPosition = &JointValueMapWaveFwd;
    nextVelocity = &JointVelocityMapWaveFwd;

    this->usingProxy("Armar3KinematicUnit");
}


void SimpleWave::onConnectComponent()
{
    kinUnit = this->getProxy<KinematicUnitInterfacePrx>("Armar3KinematicUnit");
    ARMARX_LOG << "using proxy Armar3KinematicUnit";
    usingTopic("RobotState");
    ARMARX_LOG << "using topic RobotState";

    setRobotPositionToReach(this->JointValueMapInitPose);

    wavingTask = new RunningTask<SimpleWave>(this, &SimpleWave::wave);
    wavingTask->start();
}


void SimpleWave::onDisconnectComponent()
{
    wavingTask->stop();
}


void SimpleWave::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr SimpleWave::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new SimpleWavePropertyDefinitions(
                                              getConfigIdentifier()));
}

/*
 * Return whether the angles of the currentPosition are within a margin of epsilion near the positionToReach
*/
bool SimpleWave::reachedPos()
{
    bool positionReached = true;
    for(const auto& jointValueToReach : *positionToReach)
    {
        std::lock_guard<std::mutex> lock(currentPosMutex);
        positionReached = positionReached && (this->currentPosition[jointValueToReach.first] > jointValueToReach.second - epsilon
                && this->currentPosition[jointValueToReach.first] < jointValueToReach.second + epsilon);
    }
    return positionReached;
}

/*
 * Uses the kineMaticUnit to set joint anges for the robot to reach
*/
void SimpleWave::setRobotPositionToReach(std::map<std::string, float> posToReachMap)
{
    NameControlModeMap positionControlModeMap;
    //sets to position control mode the joints in the map
    for (const auto& jointNameValue : posToReachMap)
    {
        positionControlModeMap[jointNameValue.first] = ePositionControl;
    }
    //switch to position control
    kinUnit->switchControlMode(positionControlModeMap);
    // set the angles defined by the joint target pose
    kinUnit->setJointAngles(posToReachMap);
}

/*
 * Uses the kineMaticUnit to set volicities wit the given values
*/
void SimpleWave::setRobotVelocity(std::map<std::string, float> velocityMap)
{
    NameControlModeMap velocityControlModeMap;
    for (const auto& jointVelocity : velocityMap)
    {
        velocityControlModeMap[jointVelocity.first] = eVelocityControl;
    }
    kinUnit->switchControlMode(velocityControlModeMap);
    kinUnit->setJointVelocities(velocityMap);
}

/*
 * Periodicly checks whether turning point in the waving behaviour has been reached
 * and changes the movement if so
*/
void SimpleWave::wave()
{
    while(1)
    {
        if(reachedPos())
        {
            ARMARX_LOG << "changing directions";
            setRobotVelocity(*this->nextVelocity);
            this->positionToReach = this->nextPosition;
            if (this->nextVelocity == &this->JointVelocityMapWaveFwd)
            {
                this->nextVelocity = &this->JointVelocityMapWaveBack;
                this->nextPosition = &this->JointValueMapWaveBack;
            }
            else
            {
                this->nextVelocity = &this->JointVelocityMapWaveFwd;
                this->nextPosition = &this->JointValueMapWaveFwd;
            }
        }

        usleep(1000);
    }
}

void SimpleWave::reportControlModeChanged(const NameControlModeMap& jointModes, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{

}

void SimpleWave::reportJointAngles(const NameValueMap& jointAngles, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{
    //TODO only if aValueChanged
    //ARMARX_LOG << "got joint angles from the kinUnit";
    std::lock_guard<std::mutex> lock(currentPosMutex);
    this->currentPosition = jointAngles;
}


void SimpleWave::reportJointVelocities(const NameValueMap& jointVelocities, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{

}

void SimpleWave::reportJointTorques(const NameValueMap& jointTorques, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{

}

void SimpleWave::reportJointAccelerations(const NameValueMap& jointAccelerations, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{

}

void SimpleWave::reportJointCurrents(const NameValueMap& jointCurrents, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{

}

void SimpleWave::reportJointMotorTemperatures(const NameValueMap& jointMotorTemperatures, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{

}

void SimpleWave::reportJointStatuses(const NameStatusMap& jointStatuses, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c)
{

}

