/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SimpleWavingExample::ArmarXObjects::SimpleWave
 * @author     [Author Name] ( [Author Email] )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_SimpleWavingExample_SimpleWave_H
#define _ARMARX_COMPONENT_SimpleWavingExample_SimpleWave_H


#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <mutex>

namespace armarx
{
    /**
     * @class SimpleWavePropertyDefinitions
     * @brief
     */
    class SimpleWavePropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SimpleWavePropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-SimpleWave SimpleWave
     * @ingroup SimpleWavingExample-Components
     * A description of the component SimpleWave.
     * 
     * @class SimpleWave
     * @ingroup Component-SimpleWave
     * @brief Brief description of class SimpleWave.
     * 
     * Detailed description of class SimpleWave.
     */
    class SimpleWave :
        virtual public armarx::Component,
        virtual public Observer,
        virtual public KinematicUnitObserverInterface
    {
    public:
        // slice interface implementation
        void reportControlModeChanged(const NameControlModeMap& jointModes, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointAngles(const NameValueMap& jointAngles, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointVelocities(const NameValueMap& jointVelocities, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointTorques(const NameValueMap& jointTorques, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointAccelerations(const NameValueMap& jointAccelerations, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c);
        void reportJointCurrents(const NameValueMap& jointCurrents, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointMotorTemperatures(const NameValueMap& jointMotorTemperatures, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointStatuses(const NameStatusMap& jointStatuses, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c = ::Ice::Current());

        void onInitObserver(){};
        void onConnectObserver(){};


        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "SimpleWave";
        }

        void wave();



    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        std::mutex currentPosMutex;

        bool reachedPos();
        void setRobotPositionToReach(std::map<std::string, float> posToReach);
        void setRobotVelocity(std::map<std::string, float> velocity);

        //joint positions and velocities
        const std::map<std::string, float> JointValueMapInitPose = {{"Shoulder 1 L", -1.25}, {"Shoulder 2 L", 1.5}, {"Elbow L", -1.38}, {"Underarm L", 1.0}};
        const std::map<std::string, float> JointValueMapWaveBack = {{"Elbow L", -0.8}};
        const std::map<std::string, float> JointValueMapWaveFwd = {{"Elbow L", 0.3}};
        const std::map<std::string, float> JointVelocityMapWaveBack = {{"Elbow L", -1.0}};
        const std::map<std::string, float> JointVelocityMapWaveFwd = {{"Elbow L", 1.0}};

        std::map<std::string, float> currentPosition = {{"Shoulder 1 L", 10}, {"Shoulder 2 L", 10}, {"Elbow L", 10}, {"Underarm L", 10}};
        const std::map<std::string, float>* positionToReach;
        const std::map<std::string, float>* nextPosition;
        const std::map<std::string, float>* nextVelocity;

        KinematicUnitInterfacePrx kinUnit;

        int maxWaves = 100;
        int waveCount = 0;
        float epsilon = 0.05f;

        RunningTask<SimpleWave>::pointer_type wavingTask;
    };
}

#endif
